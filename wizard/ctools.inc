<?php

/**
 * @file ctools.inc
 * Includes functions additions that would be moved to the wizard.inc file if adopted.
 */


define('CTOOLS_WIZARD_CACHE_NAME', 'ctools_wizard_cache');

/**
 * Include ctools wizard and object cache.
 */
ctools_include('wizard');
ctools_include('object-cache');

/**
 * Get a wizard procesed wizard form.
 */
function ctools_get_wizard($wizard_id) {
  ctools_include('wizard');

  $args = func_get_args();

  $wizard = call_user_func_array('ctools_retrieve_wizard', $args);

  array_shift($args);

  ctools_prepare_wizard($wizard_id, $wizard, $trail);

  ctools_render_wizard($wizard_id, $wizard, $trail);

//  return theme($wizard['theme'], $wizard, $wizard_id);
  return theme_ctools_wizard($wizard['output'], $wizard);
}

function ctools_render_wizard($wizard_id, &$wizard, $trail) {
  $item = menu_get_item();

  if (!isset($wizard['path'])) {
    $args = split('/', $item['path']);
    $trail = array_diff($item['map'], $args);
    $step = array_pop($trail);
    array_push($trail, '%step');  
    $wizard['path'] = join('/', array_merge($args, $trail));
    $wizard['step'] = $step;
  }

  // Don't override #theme if someone already set it.
  if (!isset($wizard['theme'])) {
    init_theme();
    $registry = theme_get_registry();
    if (isset($registry[$wizard_id])) {
      $wizard['theme'] = $wizard_id;
    }
  }
  
  // This stuff should be moved to ctools defaults
  $wizard += array(
    'next text' => t('Continue'),
    'next callback' => 'ctools_wizard_next',
    'finish callback' => 'ctools_wizard_finish',
    'finished callback' => 'ctools_wizard_finished',
    'return callback' => 'ctools_wizard_finish',
    'cancel callback' => 'ctools_wizard_cancel',
  );
  if (! empty($wizard['finish']['batch'])) {
    $wizard['finish']['batch'] += array(
      'finished' => 'ctools_wizard_batch_finished',
    );
  }

  // Initialize our form state, return to first step if it fails
  if (! $form_state = ctools_wizard_init($wizard['id'], $step, $wizard)) {
    drupal_goto($item['path']);
  }

  if (! empty($wizard['advanced wizard'])) {
//    $wizard['output'] = ctools_wizard_advanced_form($
  }
  else {
    $wizard['output'] = ctools_wizard_multistep_form($wizard, $step, $form_state);
  }

  if (! isset($wizard['theme'])) {
    $wizard['theme'] = 'ctools_wizard';
  }
}

function ctools_wizard_get_trail(&$wizard) {
  $item = menu_get_item();

  $args = split('/', $item['path']);
  $trail = array_diff($item['map'], $args);
  $step = array_pop($trail);
  array_push($trail, '%step');

//  $wizard['path'] = join('/', array_merge($args, $trail));

  return $step;
}

function theme_ctools_wizard($output, $wizard) {
  return $output;
}

function ctools_prepare_wizard($wizard_id, &$wizard) {
  $data =&$wizard;
  drupal_alter('ctools_wizard_' . $wizard_id, $wizard);

  drupal_alter('ctools_wizard', $wizard, $wizard_id);
}

function ctools_retrieve_wizard($wizard_id) {
  static $wizards;

  $args = func_get_args();
  array_shift($args);

  if (! function_exists($wizard_id)) {
    if (!isset($wizards) || !isset($wizards[$wizard_id])) {
      $wizards = module_invoke_all('ctools_wizards', $wizard_id, $args);
    }
    $wizard_definition = $wizards[$wizard_id];
    if (isset($wizard_definition['callback arguments'])) {
//      $args = array_merge($wizard_definition['callback arguments'], $args);
    }
    if (isset($wizard_definition['callback'])) {
      $callback = $wizard_definition['callback'];
    }
  }

  $wizard = call_user_func_array(isset($callback) ? $callback : $wizard_id, $args);
  $wizard['id'] = $wizard_id;
  $wizard['parameters'] = $saved_args;

  return $wizard;
}

function ctools_wizard_advanced_form($wizard, $args, $form_state) {
}

/**
 * Function for initializing a wizard.
 */
function ctools_wizard_init($cache_name, &$step, $wizard) {
  
  $wizard_data = ctools_wizard_get_cache($cache_name);

  // If we don't have any data but we have a step, go back.
  if (! $wizard_data && $step != '') {
    // Allow redirection in the form setup if initialization failed
    return FALSE;
  }

  $form_state = !empty($wizard['form state']) ? $wizard['form state'] : array();

  // Initialize wizard data
  if (! $wizard_data) {
    $step = current(array_keys($wizard['forms']));
    $wizard_data = array();
    ctools_wizard_set_cache($cache_name, $wizard_data);
  }
  $form_state['wizard_data'] = $wizard_data;
  $form_state['cache name'] = $cache_name;

  if (isset($wizard['finish'])) {
    $form_state['finish'] = $wizard['finish'];
  }

  return $form_state;
}

/**
 *
 */
function ctools_wizard_set_value($value, $key, &$form_state) {
  if (! isset($form_state['wizard_data'])) {
    $form_state['wizard_data'] = array();
  }
  $form_state['wizard_data'][$key] = $value;
}

function ctools_wizard_get_value($key, $form_state) {
  return isset($form_state['wizard_data'][$key]) ? $form_state['wizard_data'][$key] : NULL;
}

/**
 * Shortcut function for checking if a wizard form was canceled.
 * Typically used to prevent validation.
 */
function ctools_wizard_check_canceled($form_state) {
  return $form_state['clicked_button']['#wizard type'] == 'cancel';
}


/**
 * Form callbacks
 */

/**
 * Default "next callback" for a wizard form.
 */
function ctools_wizard_next(&$form_state) {
  ctools_wizard_set_cache($form_state['cache name'], $form_state['wizard_data']);
}

/**
 * Default "cancel callback" for a wizard form.
 */
function ctools_wizard_cancel(&$form_state) {
  // Clear the stored data
  ctools_wizard_clear_cache($form_state['cache name']);

  // Determine where to redirect
  $form_state['redirect'] = $form_state['cancel redirect'] ? $form_state['cancel redirect'] : '<front>';

  // Display a message to the user.
  if ($form_state['cancel message']) {
    drupal_get_messages();
    drupal_set_message($form_state['cancel message']);
  }
}

function ctools_wizard_finish(&$form_state) {
  if (! empty($form_state['finish']['batch'])) {
    // Prepare batch operations
    $operations = array();
    foreach ($form_state['finish']['batch']['operations'] AS $op => $vars) {
      $values = array();
      foreach ($vars AS $key) {
        $values[] = ctools_wizard_get_value($key, $form_state);
      }
//      $values[] =& $form_state;
      $operations[] = array($op, $values);
    }
    // Set up batch process
    $batch = array('operations' => $operations) + $form_state['finish']['batch'];

    // Do we have to clear the cache here or can we clear it in the "finished callback"?
    ctools_wizard_clear_cache($form_state['cache name']);

    batch_set($batch);
    batch_process();
  }
  else {
    // Add non batch handling
    $function = $form_state['finish']['callback'];

    if (! empty($form_state['finish']['arguments'])) {
      $values = array();
      foreach ($form_state['finish']['arguments'] AS $key) {
        $values[] = ctools_wizard_get_value($key, $form_state);
      }
      $values[] = $form_state;
      call_user_func_array($function, $values);
    }
    else {
      $function($form_state);
    }

    // Do we have to clear the cache here or can we clear it in the "finished callback"?
    ctools_wizard_clear_cache($form_state['cache name']);
    
    drupal_redirect_form(array('#redirect' => $form_state['wizard']['redirect']), $form_state['redirect']);
  }
}

/**
 * Wizard cache functions.
 */

function ctools_wizard_set_cache($form_cache_name, $wizard_data) {
  ctools_object_cache_set(CTOOLS_WIZARD_CACHE_NAME, $form_cache_name, $wizard_data);
}

/**
 * Retrieve the wizard's form cache.
 */
function ctools_wizard_get_cache($form_cache_name) {
  $cache = ctools_object_cache_get(CTOOLS_WIZARD_CACHE_NAME, $form_cache_name);
  return $cache;
}

/**
 * Clear the wizard's form cache.
 */
function ctools_wizard_clear_cache($form_cache_name) {
  ctools_object_cache_clear(CTOOLS_WIZARD_CACHE_NAME, $form_cache_name);
}


function ctools_wizard_batch_finished($success, $results, $operations) {
  if (count($results)) {
    foreach ($results AS $result) {
  //    drupal_set_message($result, $success ? 'status' : 'warning'); 
    }
  }
}
