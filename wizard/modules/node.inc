<?php

$plugin = array(
  'hook menu alter' => 'wizard_node_menu_alter',
);

/**
 * Implementation of hook_ctools_wizards().
 */
function node_ctools_wizards($form_id) {
  $wizards = array();
  if ($types = node_get_types()) {
    foreach (array_keys($types) AS $type) {
      $wizards[$type . '_node_wizard']['callback'] = 'node_wizard';
    }
  }
  return $wizards;
}

function wizard_node_menu_alter(&$items) {
return;
  foreach (node_get_types('types', NULL, TRUE) as $type) {
    $add_path = 'node/add/' . str_replace('_', '-', $type->type);
    $items[$add_path] = array_merge($items[$add_path], array(
      'page callback' => 'node_add_wizard',
      'file path' => drupal_get_path('module', 'wizard') . '/modules',
      'file' => 'node.inc',
    ));
  }
}

/**
 * Implementation of hook_ctools_wizards().
 */
function wizard_node_forms() {
  $wizards = array();
  if ($types = node_get_types()) {
    foreach (array_keys($types) as $type) {
      $wizards[$type . '_node_wizard_form']['callback'] = 'node_wizard_form';
    }
  }
  return $wizards;
}

/**
 *
 */
function node_wizard($node) {
  return array(
    'show back' => TRUE,
    'show trail' => FALSE,
    'form state' => array(
      'node' => $node,
    ),
    'finish' => array(
      'callback' => 'node_wizard_save',
      'arguments' => array('node_form', 'values'),
    ),
    'forms' => array(
      'node' => array(
        'title' => $node->nid ? $node->title : t('Create @name', array('@name' => node_get_types('name', $node))),
//        'form id' => $node['type'] . '_node_wizard_form',
        'form id' => 'node_wizard_form',
      ),
      'preview' => array(
        'title' => t('Prevew @name', array('@name' => node_get_types('name', $node))),
        'form id' => 'node_preview_wizard_form',
      ),
    ),
//    'wrapper callback' => 'node_wizard_form_wrapper',
  );
}

function node_add_wizard($type) {
  global $user;

  $types = node_get_types();
  $type = isset($type) ? str_replace('-', '_', $type) : NULL;
  // If a node type has been specified, validate its existence.
  if (isset($types[$type]) && node_access('create', $type)) {
    // Initialize settings:
    $node = array(
      'uid' => $user->uid,
      'name' => (isset($user->name) ? $user->name : ''),
      'type' => $type,
      'language' => '',
    );

    drupal_set_title(t('Create @name', array('@name' => $types[$type]->name)));
    return ctools_get_wizard($type . '_node_wizard', $node);
  }
  return drupal_not_found();
}

function node_wizard_form(&$form, &$form_state) {
  $form = array_merge(node_form($form_state, $form_state['node']), $form);
  drupal_alter('form_' . $form_state['node']['type'] . '_node_form', $form, $form_state);
  drupal_alter('form', $form, $form_state, $form_state['node']['type'] . '_node_form');
}

function node_wizard_form_validate(&$form, &$form_state) {
  module_load_include('pages.inc', 'node');
  node_form_validate($form, $form_state);
}

function node_wizard_form_submit(&$form, &$form_state) {
  ctools_wizard_set_value($form_state['values'], 'values', $form_state);
  ctools_wizard_set_value($form, 'node_form', $form_state);
}

function node_preview_wizard_form(&$form, &$form_state) {
  $values = ctools_wizard_get_value('values', $form_state);
  $form['preview'] = array(
    '#value' => node_preview((object)$values),
  );
}

function node_wizard_save($node_form, $values) {
  $node_form_state = array('values' => $values);
  module_load_include('pages.inc', 'node');
  node_form_submit($node_form, $node_form_state);
}
